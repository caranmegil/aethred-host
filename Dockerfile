FROM node:16

WORKDIR /usr/aethredweb/
COPY . /usr/aethredweb
EXPOSE 5000
RUN npm ci
RUN npm run build 
CMD ["node", "./index.js"]

