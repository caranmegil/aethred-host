/*

index.js - The driver behind the Aethred chat bot web host.
Copyright (C) 2022  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dotenv/config';

import express from 'express';
import bodyParser from 'body-parser';
import fetch from 'node-fetch';

const AETHRED_HOST = process.env.AETHRED_HOST;
const PORT = process.env.PORT || 5000;

let app = express();
app.use(bodyParser.json());

app.use('/', express.static('dist'));

app.post('/speak', async function(req, res) {
  const resp = await fetch(AETHRED_HOST, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({'text': req.body.message}),
  });
  const data = await resp.json();
  const response = data.response;
  res.json({response});
});

app.post('/fedi', async function(req, res) {
  const message = req.body.message
});


app.listen(PORT, () => {
  console.log(`Aethred host listening on port ${PORT}`);
});
